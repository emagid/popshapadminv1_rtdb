var config = {
    apiKey: 'AIzaSyARbWBB8gJ8AYQO1rNwn7T38gwuJdV9kEY',
    authDomain: 'encoded-blend-193619.firebaseapp.com',
    projectId: 'encoded-blend-193619',
    databaseURL: 'https://encoded-blend-193619.firebaseio.com',
    storageBucket: 'encoded-blend-193619.appspot.com',
    messagingSenderId: '495405917615'
};
var curr_user;
var db;
$(document).ready(function(){
    firebase.initializeApp(config);
    db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            db.collection('users').doc(user.uid).get().then(function(_user){
                curr_user = _user.data();
                curr_user.displayName = firebase.auth().currentUser.displayName; 
                curr_user.phoneNumber = firebase.auth().currentUser.phoneNumber; 
                init();
            });
        } else {
          if(window.location.pathname != "/admin/login"){     
            window.location.href = "/admin/login/logout";
          } else {
            init();
          }
        }
    });
 });
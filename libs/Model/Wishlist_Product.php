<?php

class Wishlist_Product extends \Emagid\Core\Model {

    static $tablename = 'wishlist_product';

    static $fields = [
        'quantity',
        'product_map_id',
        'wishlist_id'
    ];
}

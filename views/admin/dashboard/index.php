<div class="content">
	<div class="container-fluid">
		<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-primary">
					<h4 class="card-title" id="profile_details" >Profile details</h4>
					<p class="card-category" >Complete your profile</p>
				</div>
				<div class="card-body">
					<form id='user_form'>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="bmd-label-floating">Company (Must be set by admin)</label>
									<input type="text" name="client" class="form-control" disabled>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="bmd-label-floating">Email address</label>
									<input type="email" name='email' class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="bmd-label-floating">Phone Number</label>
									<input type="phone" name="phoneNumber" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="bmd-label-floating">DisplayName</label>
									<input type="text" name="displayName" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="bmd-label-floating">New Password</label>
									<input type="password" name="password" class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Notes</label>
									<div class="form-group">
									<label class="bmd-label-floating">Floating text</label>
									<textarea class="form-control" rows="5" id="notes"></textarea>
									</div>
								</div>
							</div>
						</div>
						<input type="submit" id="sign-in-button" class="btn btn-primary pull-right" value="Update Profile" />
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var fUser;
	function init(){
		firebase.auth().useDeviceLanguage();
		// window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
		// 	'size': 'invisible',
		// 	'callback': function(response) {
		// 		debugger
		// 	}
		// });
		$('#user_form :input[name=email]').val(curr_user.email).siblings('label').addClass(curr_user.email?'bmd-label-static':'');
		$('#user_form :input[name=displayName]').val(curr_user.displayName).siblings('label').addClass(curr_user.displayName?'bmd-label-static':'');;
		$('#user_form :input[name=phoneNumber]').val(curr_user.phone).siblings('label').addClass(curr_user.phone?'bmd-label-static':'');;
		$('#notes').val(curr_user.notes);

		$('#user_form').submit(function(e){
			e.preventDefault();
			fUser = firebase.auth().currentUser;
			Promise.all([updateProfile(),
			updateEmail(),
			// updatePhoneNumber(),
			updatePassword()])
			.then(function(values){
				$('#profile_details').text('Profile Updated');
				db.collection('users').doc(firebase.auth().currentUser.uid).set({
					user_role: firebase.auth().currentUser.email.includes('@emagid.com')?'superadmin':'client',
					email: firebase.auth().currentUser.email,
					phone: $('#user_form [name=phoneNumber]').val()	?$('#user_form [name=phoneNumber]').val():firebase.auth().currentUser.phoneNumber,
					notes: $('#notes').val()
				},{ merge: true })
			}).catch(function(error){
				alert('Update Error: '+error)
			});

		});

		function updateProfile(){
			var formData = {
				displayName: $('#user_form [name=displayName]').val(),
			}
			fUser.updateProfile(formData).then(function() {
				$('#profile_details').text('updated!');
				return Promise.resolve(true);
			}).catch(function(error) {
				debugger
				return Promise.reject(error);
			})
		}
		function updatePassword(){
			if($('#user_form [name=password]').val()){
				return fUser.updatePassword($('#user_form [name=password]').val());
			} else {
				return Promise.resolve(true);
			}
		}
		function updatePhoneNumber(){
			if($('#user_form [name=phoneNumber]').val()){
				if(fUser.phoneNumber){
					return fUser.updatePhoneNumber($('#user_form [name=phoneNumber]').val());
				} else {
					return fUser
					.linkWithPhoneNumber($('#user_form [name=phoneNumber]').val(), window.recaptchaVerifier)
					.then(function (confirmationResult) {
						let code = prompt("Enter the phone verification code!");
						var credential = firebase.auth.PhoneAuthProvider.credential(confirmationResult.verificationId, prompt("Enter the phone verification code!"));
						return fUser.linkAndRetrieveDataWithCredential(credential);
					}).catch(function (error) {
					// Error; SMS not sent
					// ...
					});

					var credential = firebase.auth.PhoneAuthProvider.
					firebase.auth().signInWithCredentials()
				}
			} else {
				return Promise.resolve(true);
			}
		}
		function updateEmail(){
			if($('#user_form [name=email]').val()){
				if(fUser.email){
					return fUser.updateEmail($('#user_form [name=email]').val());
				} else {

				}
			} else {
				return Promise.resolve(true);
			}
		}
	}


	// var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	// var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	// var dataSet = {"previous":{"2014-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-05":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-06":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-09":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}},"current":{"2015-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-05":{"monthly_sales":"4078.22","gross_margin":0,"monthly_orders":"8"},"2015-06":{"monthly_sales":"9485.6","gross_margin":0,"monthly_orders":"25"},"2015-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-09":{"monthly_sales":"35","gross_margin":0,"monthly_orders":"1"},"2015-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}}};

	// var months = [];
	// for (key in dataSet.previous){
	// 	var month = new Date(key.split('-')[0], key.split('-')[1]-1);
	// 	months.push(monthNames[month.getMonth()]);
	// }

	// var set = [[],[]];
	// for (key in dataSet.previous){
	// 	set[0].push(dataSet.previous[key].monthly_sales);
	// }
	// for (key in dataSet.current){
	// 	set[1].push(dataSet.current[key].monthly_sales);
	// }
	// var barChartData1 = {
	// 	labels : months,
	// 	datasets : [
	// 		{
	// 			fillColor : "rgba(220,220,220,0.5)",
	// 			strokeColor : "rgba(220,220,220,0.8)",
	// 			highlightFill: "rgba(220,220,220,0.75)",
	// 			highlightStroke: "rgba(220,220,220,1)",
	// 			data : set[0]
	// 		},
	// 		{
	// 			fillColor : "rgba(151,187,205,0.5)",
	// 			strokeColor : "rgba(151,187,205,0.8)",
	// 			highlightFill : "rgba(151,187,205,0.75)",
	// 			highlightStroke : "rgba(151,187,205,1)",
	// 			data : set[1]
	// 		}
	// 	]

	// }

	// var lineChartData = {
	// 	labels : months,
	// 	datasets : [
	// 		{
	// 			label: "Previous",
	// 			fillColor : "rgba(220,220,220,0.2)",
	// 			strokeColor : "rgba(220,220,220,1)",
	// 			pointColor : "rgba(220,220,220,1)",
	// 			pointStrokeColor : "#fff",
	// 			pointHighlightFill : "#fff",
	// 			pointHighlightStroke : "rgba(220,220,220,1)",
	// 			data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
	// 		},
	// 		{
	// 			label: "Current",
	// 			fillColor : "rgba(151,187,205,0.2)",
	// 			strokeColor : "rgba(151,187,205,1)",
	// 			pointColor : "rgba(151,187,205,1)",
	// 			pointStrokeColor : "#fff",
	// 			pointHighlightFill : "#fff",
	// 			pointHighlightStroke : "rgba(151,187,205,1)",
	// 			data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
	// 		}
	// 	]

	// }

	// set = [[],[]];
	// for (key in dataSet.previous){
	// 	set[0].push(dataSet.previous[key].monthly_orders);
	// }
	// for (key in dataSet.current){
	// 	set[1].push(dataSet.current[key].monthly_orders);
	// }
	// var barChartData2 = {
	// 	labels : months,
	// 	datasets : [
	// 		{
	// 			fillColor : "rgba(220,220,220,0.5)",
	// 			strokeColor : "rgba(220,220,220,0.8)",
	// 			highlightFill: "rgba(220,220,220,0.75)",
	// 			highlightStroke: "rgba(220,220,220,1)",
	// 			data : set[0]
	// 		},
	// 		{
	// 			fillColor : "rgba(151,187,205,0.5)",
	// 			strokeColor : "rgba(151,187,205,0.8)",
	// 			highlightFill : "rgba(151,187,205,0.75)",
	// 			highlightStroke : "rgba(151,187,205,1)",
	// 			data : set[1]
	// 		}
	// 	]

	// }

	// window.onload = function(){
	// 	var ctxB = document.getElementById("B").getContext("2d");
	//  	window.myLine = new Chart(ctxB).Line(lineChartData, {
	//  		responsive: true
	//  	});
	// 	var ctxA = document.getElementById("A").getContext("2d");
	// 	window.myBar = new Chart(ctxA).Bar(barChartData1, {
	// 		responsive : true
	// 	});
	// 	var ctxC = document.getElementById("C").getContext("2d");
	// 	window.myBar = new Chart(ctxC).Bar(barChartData2, {
	// 		responsive : true
	// 	});
	// }

</script> 
 
<?php echo footer(); ?>
<? foreach($model->navs as $page) { ?>
<li class="nav-item <?= $this->emagid->route['controller'] == $page?'active':'' ?>">
    <a class="nav-link" href="/admin/<?=$page?>">
        <i class="material-icons"><?=$page?></i>
        <p><?=ucfirst($page)?></p>
    </a>
</li>
<? } ?>
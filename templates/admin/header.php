<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="/admin/dashboard">Profile</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                  <? if(isset($_SESSION['uid'])) {?>
                <a class="nav-link" onClick="firebase.auth().signOut()">
                  <i class="material-icons">notifications</i> Log Out
                </a>
                  <? } ?>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
<footer class="container-fluid">
    <nav class="float-left">
    <ul>
        <li>
            Popshap
        </li>
    </ul>
    </nav>
    <div class="copyright float-right">
        <script>
            document.write(new Date().getFullYear())
        </script>
    </div>
    <!-- your footer here -->
</footer>
<?php

class faqController extends siteController
{

    public function index(Array $params = [])
    {
        //should run under /pages/{slug} format
        $slug = 'faq';
        $this->viewData->faq = \Model\Page::getItem(null,['where'=>"slug = '{$slug}'"]);

        $this->configs['Meta Title'] = "FAQ";
        $this->loadView($this->viewData);
    }

}
<?php

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class homeController extends siteController {
    function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){

        $firebase = $this->firebase;
        $database = $this->database;

        $accountid  = $this->accountid;
        $eventid    = $this->eventid;
        $kiosk      = $this->kiosk;

        $account_ref = $database->getReference("account/$accountid");
        $account = $account_ref->getValue();
        $images = [];
        $event_ref = $database->getReference("event/$eventid");
        $event = $event_ref->getValue();

        $kioskSession_q = $database->getReference('kioskSession')
            ->orderByChild('kioskId')
            ->equalTo($kiosk);
        $kSess_results = $kioskSession_q->getValue();
//        var_dump($kioskSession_ref->getSnapshot());
//        $kioskSessionid = $kioskSession_ref->
        $in_event = in_array(
            array_keys($kSess_results)[0],
            array_keys($event['kioskSession'])
        );
        $pics = [];
        if($in_event){
            $keyS = array_keys($kSess_results)[0];
            $this->kioskSession_id = $keyS;
            $_SESSION['kioskSession'] = $keyS;
            foreach ($kSess_results[$keyS]['photos'] AS $pid => $photo){
                $photoO = $database->getReference("kioskSession/$keyS/photos/$pid")->getUri();
                var_dump($photoO);
                if($photo['isApproved'] && $photo['ema']){
//                if(true){
                    $pics[] = $photo['url'];
                }
            }
        }

        $this->viewData->pics = $pics;


//        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
//        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
//        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);
//
//        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
//        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);

        $this->loadView($this->viewData);
    }
}
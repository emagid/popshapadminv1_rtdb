<?php

use Emagid\Core\Membership;
use Firebase\JWT\JWT;

/**
 * Base class to control login
 */

class loginController extends \Emagid\Mvc\Controller {
	
	public function login(){
		if(isset($_SESSION['uid'])){
			redirect('/admin/dashboard/index');
		}

		$model = new \stdClass();
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}
	
	public function login_post()
	{	
		if (\Model\Admin::login($_POST['username'],$_POST['password'])){
			redirect(ADMIN_URL . 'dashboard/index');
		} 
		/* 
		$admin = new \Model\Admin();
		$admin->first_name = 'Master';
		$admin->last_name = 'User';
		$admin->email = 'master@user.com';
		$admin->username = 'emagid';
		$admin->password = 'test5343';
		$admin->permissions = 'Administrators';

		$hash = Membership::hash($admin->password);
		$admin->password = $hash['password'];
		$admin->hash = $hash['salt'];

		if ($admin->save()){
			$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and role_id = 1 and admin_id = '.$admin->id]);
			if (count($adminRoles) <= 0){
				$adminRole = new \Model\Admin_Roles();
				$adminRole->role_id = 1;
				$adminRole->admin_id = $admin->id;
				$adminRole->save();
			}
		};
		echo 'BITCH';*/
		
		$model = new \stdClass();
		$model->errors = ['Invalid username or password'];
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}

	public function logout(){
		Membership::destroyAuthenticationSession();
		unset($_SESSION['uid']);
		redirect(ADMIN_URL . 'login');
	}

	public function setToken_post(){
		$uid = $_POST['uid'];
		$uidToken = $_POST['uid_token'];

		$status = $this->verify_token($uidToken,$uid);

		if($status){
			$_SESSION['uid'] = $uid;
		}

		$this->toJson(['status'=> $status]);
	}

	function verify_token($token,$uid) {
		
		/*
		 * SERVER timezone issue
		 */
		
		// $service_account_email = "abc-123@a-b-c-123.iam.gserviceaccount.com";
		// $key = "-----BEGIN CERTIFICATE-----\nMIIDHDCCAgSgAwIBAgIIch09slt+C7swDQYJKoZIhvcNAQEFBQAwMTEvMC0GA1UE\nAxMmc2VjdXJldG9rZW4uc3lzdGVtLmdzZXJ2aWNlYWNjb3VudC5jb20wHhcNMTgx\nMTE3MjEyMDQzWhcNMTgxMjA0MDkzNTQzWjAxMS8wLQYDVQQDEyZzZWN1cmV0b2tl\nbi5zeXN0ZW0uZ3NlcnZpY2VhY2NvdW50LmNvbTCCASIwDQYJKoZIhvcNAQEBBQAD\nggEPADCCAQoCggEBAKiVRkcBERmxcplxLwGL0YDksAPeFf4fDCvwATKoNQEjtvc7\nMADQgibT6uZoSYi2e0hxpwKCk9R4wPzGYigOz3XYNqa0Y0GWOXgElTmpSmYooivc\ngKJl4BKKye3fCvo7UXnrDocEWp1kOMDjRvMi7UkiJHUfb8XyLfIGWJhWYkROiwOG\nrXs6AGh48n+/GhT2GvWA32oxKh2lF73U0JiuT2i9U9ZA/Gz5TYdVmRtnvJNroZo8\nlrxDWSbwUhpUuZSFBEGaazxJi0herblsZwwaveX7yaxKov0Wp1P6YCnm5AVH1EoC\nyWv8jts1HXROKFHBkBV1BorDRmLuPaFFjzjzZbsCAwEAAaM4MDYwDAYDVR0TAQH/\nBAIwADAOBgNVHQ8BAf8EBAMCB4AwFgYDVR0lAQH/BAwwCgYIKwYBBQUHAwIwDQYJ\nKoZIhvcNAQEFBQADggEBAFoiuISaKRneJ8A1q/aVOA7RmA7uS+xKXSWnIaue1R7o\nnDV5DAE2+A3o2xEmR2PMqDOqAXFHAJ0mdeJ7Ql+qRoHg8MUw8gmU6TnsV+wzfwSU\nyP9+4XXzca2ueVP9lXMDFv/uIGiUJQ2d+oTyJO6fPjS9YyRlV/fY5MXdvlJF8Cz6\nApSMU6Eij7dXNg92Y2LLGoXxWNEr20K1DYdOCcmbiisk7vlvLZEfZk7TNrSVtU/e\nMrLjC8vJrJIh/O+tNs0MouR4pgBK2teBqD0q3JQRPAtsN3wYmAl2Y5L4UfWH5mj5\n1IGjDAk40e+raBXHLPT3/J7ZShuczZcXpn188rVVwUA=\n-----END CERTIFICATE-----\n";

		// $decoded = JWT::decode($token, $key, ["RS256"]);
		
		// $now_seconds = time();

		// if($decoded->exp <= $now_seconds){
		// 	return false;
		// }
		// if($decoded->iat > $now_seconds){
		// 	return false;
		// }
		// if($decoded->auth_time > $now_seconds){
		// 	return false;
		// }
		// if($decoded->iss != "https://securetoken.google.com/encoded-blend-193619"){
		// 	return false;
		// }
		// if($decoded->aud != "encoded-blend-193619"){
		// 	return false;
		// }
		// if($decoded->sub != $uid || $decoded->user_id  != $uid){
		// 	return false;
		// }

		// $now_seconds = time();

		// $payload = array(
		//   "iss" => "https://securetoken.google.com/encoded-blend-193619",
		//   "sub" => $uid,
		//   "aud" => "encoded-blend-193619",
		//   "iat" => $now_seconds,
		//   "auth_time" => $now_seconds,
		//   "exp" => $now_seconds+(60*60),  // Maximum expiration time is one hour
		//   "claims" => array(
		// 	"kid" => "359bcdcfe662d3f1c09de1cba30d99f7f4f98d69"
		//   )
		// );

		return true;
	}

	public function toJson($array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
        exit();
    }

}